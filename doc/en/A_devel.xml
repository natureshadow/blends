  <appendix id="DevelDescription">
  <title>Description of development tools</title>
  <sect1 id="blends-dev">
  <title>Package <package>blends-dev</package></title>

<para>
If metapackages are built using the tools inside the
<package>blends-dev</package> package it can be ensured that the
resulting metapackages will work nicely with the same version of
<package>blends-common</package> package.  The goal is to keep
necessary changes for the source of the metapackages of a Debian Pure
Blend as low as possible when the version of the
<package>blends</package> source package changes.  Thus it is
strongly recommended to use the tools described below.
</para>
<para>
The usage of the tools in the <package>blends-dev</package> package might
introduce a versioned dependency in the
<varname>&lt;blend&gt;</varname>-<package>config</package> package from which
all other metapackages of the <varname>Blend</varname> in question will
depend. This <varname>&lt;blend&gt;</varname>-<package>config</package> package
instantiates the <varname>Blend</varname> in the common registry for all Blends in
<filename>/etc/blends</filename>.
</para>

<para>
The version <package>0.7.0</package> of <package>blends-dev</package> uses 
<ulink url="https://wiki.debian.org/UltimateDebianDatabase">UDD</ulink> 
to generate Blends' metapackages. Currently all Blends' info is stored into UDD. 
Information such as VCs, description, homepage etc for a Blend can be found into the 
<ulink url="http://udd.debian.org/schema/udd.html#public.table.blends-metadata">blends_metadata</ulink> 
UDD table. All the info about Blends' tasks and their package dependencies are also stored 
into the <ulink url="http://udd.debian.org/schema/udd.html#public.table.blends-tasks">blends_tasks</ulink> and 
<ulink url="http://udd.debian.org/schema/udd.html#public.table.blends-dependencies-alternatives">
blends_dependencies_alternatives</ulink> 
tables. Having the latter in combination with other UDD tables 
(such as a table with info about all Debian available packages) 
provides the ability to check whether a package exists for an architecture or not thus 
<package>blends-dev</package> can generate architecture dependent metapackages.
</para>

<para>
The best idea to use the tools provided by the
<package>blends-dev</package> is to put a <filename>Makefile</filename> into the
build directory containing one single line

<informalexample>
  <programlisting>
   include /usr/share/blends-dev/Makefile
 </programlisting>
</informalexample>

(see <filename>/usr/share/doc/blends-dev/examples/Makefile</filename>).  
Users using <package>blends-dev 0.7.0</package> on existing Blends 
which have more than one releases might encounter some <filename>Makefile</filename> 
errors for more info see <xref linkend="statusdump"/> and <xref linkend="changelogentry"/>.

Using this <filename>Makefile</filename> all tools that were contained in
<package>blends-dev</package> package versions before 0.4.  These tools
are moved to <filename>/usr/share/blends-dev/</filename> because there is no need
to call them directly.  Here is a list of the <filename>make</filename> targets.
</para>

<sect2 id="blends-tasks.desc">
  <title>Blend<package>-tasks.desc</package></title>

<para>
This target generates a <package>task-description.template</package> file. 
The template can be converted to a proper description file that is used in 
<package>tasksel</package> to enable selecting the tasks belonging to the Blend. 
The initial template contains all the needed package dependencies 
for a Blend. But because some packages might not be available for a(or multiple) 
architectures the template uses the following syntax when specifying packages:
<informalexample><programlisting> package1 [!arch1 arch2]</programlisting></informalexample> 

That says do not include the package1 in the 
taskdescription file when arch1 or arch2 is used. 
When a Blends' <package>orig.tar.gz</package> is generated, 
the initial template gets converted 
from the <package>blends-dev rules</package> file to a proper taskdescription file. 
The conversion is filtering out the packages which are not available for the 
host's (where the <package>orig.tar.gz</package> is generated) architecture. 
This make sure that the taskdescription file will not include package which are 
not available for the target architecture. 
Finally the file will be moved to the
<varname>blend</varname><package>-tasks</package>. All information 
about Blends package dependencies is obtained from the UDD. 
</para>

</sect2>
<sect2 id="debian_control">
  <title><filename>debian/control</filename></title>

<para>
The <filename>debian/control</filename> file of a Blend metapackage source
archive is auto generated out of dependencies that are specified in so
called <filename>tasks</filename> files.  The rationale behind this is to
enhance flexibility about changes inside the Debian package pool where
new packages might appear and others might be renamed.
The <filename>tasks</filename> just define which dependencies the Blend
maintainer group wants to be fulfilled and the
script <package>blend-gen-control</package> using UDD verifies whether these
dependencies exist in a specified package pool. A 
package pool can be considered as the packages available for a 
combination of distribution, component and release values. By default when 
creating metapackages debian,main,testing values are used to "create" a package pool from UDD. 
Once a Blends' dependencies are verified the <filename>debian/control</filename> file is generated 
according to the available packages. This works not only for the Debian package pool
containing the distributions stable, testing and unstable, but also
when building your metapackages against a different package pool of a
Debian based distribution.  This is for instance used to create the
SkoleLinux metapackages.
</para>

<para>
As mentioned in the previous section, using UDD in Blends' tools provides the ability to generate 
architecture dependent metapackages. Thus the generated 
<package>debian/control</package> specifies for every task source target as architecture value:
<informalexample>
  <programlisting>Architecture: any</programlisting></informalexample>

Specifying <package>any</package> indicates that the source package isn't dependent 
on any particular architecture and should compile fine on any one. To fulfil this 
in case of missing packages <package>control</package> file uses the following syntax:

<informalexample>
  <programlisting>Depends: package1 [!arch1 !arch2]</programlisting></informalexample>

If a package is not available for a specific arch, exclude it from it. So the above example says: 
depend on package1 but not when architecture arch1 or arch2 is used. More info about 
<package>debian/control</package> syntax can be found in 
<ulink url="http://www.debian.org/doc/debian-policy/">Debian Policy Manual</ulink>
</para>

<para>
The syntax of the <filename>tasks</filename> files which serve as the central
database for the information in the <filename>debian/control</filename> file
is defined by RFC822.  Some of the tags were mentioned formerly in
<xref linkend="packageslist"/> to explain how the <filename>tasks</filename> files
can be used to create the web sentinel pages. In order to write valid task
files it is mandatory to separate a task file paragraph by an empty line.
Otherwise the task pages of the web sentinel will not be built correctly. Now the tags that
influence the creation of the <filename>debian/control</filename> file are given.
</para>
<para>
      <variablelist>

        <varlistentry>
        <term>Depends</term>
        <listitem><para>Will be turned to "Recommends" in the
              resulting <filename>debian/control</filename> file.  The
              rationale behind this is to enable installing the
              metapackage even if a package belonging to this task is
              missing for whatever reason.  It also allows finally to
              remove the metapackage.  This makes even more
              sense since <package>apt-get</package> considers "Recommends"
              as default installation targets.</para>
        </listitem>
        </varlistentry>

        <varlistentry>
        <term>Recommends</term>
        <listitem><para>The packages that are listed as "Recommends" in the
              tasks file should be installed on the machine where the
              metapackage is installed and which are needed to work
              on a specific task.</para>
        </listitem>
        </varlistentry>
        
        <varlistentry>
        <term>Suggests</term>
	<!-- [BA] Why would we suggest non-free?  Doesn't policy allow a non-free
	     package to specify "Enhances" to avoid this problem? 
             [AT] I have to admit that "Enhances" is new to me.  When
             reading policy I think this field is out of control of
             the metapackage developer because it has to be included
             by the package maintainer.  I'm not really convinced that
             this is a good solution - but I would follow the suggestions
             of others in this issue.
          -->
        <listitem><para>Use "Suggests" for packages of lesser importance that
              might be possibly useful, or non-free packages.</para>
	      <para>
              If a package is not available in the package pool of the
              target distribution when creating
              the <filename>debian/control</filename> file inside the meta
              package source archive any "Depends" or "Recommends" are
              turned into "Suggests" to enable a flawless installation
              of the metapackage. Generally packages are concerned as missing if 
              they do not exist into Debian main component(default is testing release). 
              Packages that are specified with
              "Suggests" will not be taken over to
              the <package>tasksel</package> control file
              (Blend<filename>-tasks.desc</filename>,
              see <xref linkend="blends-tasks.desc"/>) but only to the list of
              suggested packages of the according metapackage.</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Ignore</term>
        <listitem><para>The "Ignore" key can be used as kind of "Soft-Suggests"
              to put a package on the radar of the Blend.  Packages that
              are tagged with Ignore will not be taken over into the
              list of dependencies inside
              the <filename>debian/control</filename> file of the resulting
              metapackage neither to the Blend<filename>-tasks.desc</filename>
              control file for <package>tasksel</package> but will be taken
              over onto the installation medium of a Blend in case there
              is some space left.  This key becomes especially
              important for specifying not yet packaged software that
              might be packaged in the future (prospective packages).
              This is explained in detail in the paragraph about the
              web sentinel (see <xref linkend="packageslist"/>).</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>Avoids</term>
        <listitem><para>The "Avoids" key specifies existing packages that will
              be listed in the the <filename>debian/control</filename> file as
              "Recommends" or "Suggests" but, should not go to a
              installation medium (CD, DVD, etc.) that might be
              produced by the Blend.  A reason to avoid a package might
              be that it belongs to the non-free section.</para>
        </listitem>
      </varlistentry>

      </variablelist>
</para>
</sect2>

<para>
Example:
<informalexample>
 <programlisting>
Task: finest
Description: the finest software

Depends: foo

Depends: bar

Suggests: foobar
 </programlisting>
</informalexample>
</para>

<sect2 id="statusdump">
  <title><package>statusdump</package></title>

<para>
This target generates a json file containing the latest package dependencies 
for the selected Blend. It parses the files from the <package>tasks</package> directory 
and generates a <varname>blend</varname><package>_version.json</package> into a <package>dependency_data</package> 
directory. As <package>version</package> it gets the latest version specified in the Blend's 
<package>debian/changelog</package> file. In case the <package>dependency_data</package> directory 
does not exist into a Blend's root directory it automatically creates it.
</para>

<para>
A user can also generate a json dependencies file manually 
using the <package>tasks_diff</package> script. The script can be called 
from a Blend's root directory:

<informalexample>
  <programlisting>
 /usr/share/blends-dev/task_diff --status-dump --tasks .  --output blend_version.json
</programlisting>
</informalexample>
If the user does not specify the output argument the script by default 
will generate the json file under the <package>tasks.json</package> name in the 
current directory.
</para>

<para>
Note: in case a user needs to generate a json file for a previous release 
(rather than the latest) to get the <package>changelogentry</package> 
(see <xref linkend="changelogentry"/>) target to work, must keep the following thing in mind:

The user must provide to <package>task_diff</package> script the <emphasis>root</emphasis> directory of a previous Blend release 
(through the --task(-t) argument). He should also save the output into the <package>dependency_data</package> 
directory into the latest Blend release providing manually 
the name <varname>blend</varname><package>_version.json</package> (through the --output(-o) argument: 

<informalexample>
  <programlisting>
/usr/share/blends-dev/task_diff --status-dump -t blend/tags/previous/ -o latest_blend/dependency_data/blend_version.json
</programlisting>
</informalexample>

For example if the name of the Blend is <package>myblend</package> and the release is <package>0.2.0</package> 
then the json file must have the name <package>myblend_0.2.0.json</package>
</para>

</sect2>

<sect2 id="changelogentry">
 <title>changelogentry</title>

<para>
This target compares the latest and the previous Blend release and dumps the tasks' 
package differences. It reports the added/removed packages 
per task (or added/removed task files) 
between releases. This "report" is automatically 
added into the <filename>debian/changelog</filename> 
in the latest release section under the file's manual changes. 
In case a previous difference report 
exists, it overrides it. In case a Blend does not have more than release 
(initial release) then this target is ignored.
</para>

<para>
In order the comparison to be properly performed the 
<varname>blend</varname><filename>_version.json</filename> files for the two latest releases 
must exist under the <filename>dependency_data</filename> directory. In case any of the 
previous files is missing then the target will fail with an error 
(specifying the missing version_file). The json file for the latest 
release is automatically generated from the <package>statusdump</package> target 
so it this will not cause the problem.
</para>

<para>
This changelog entry is a new feature so the problem of this target failing 
(because of a missing json file) will appear for existing Blends which have 
more than one releases and do not have a <varname>blend</varname><filename>_version.json</filename> 
for the previous release under their <filename>dependency_data</filename> directory. 
Usually Blend's releases are tagged into the VCs, so the previous problem 
can be solved by generating the dependency json file for the previous 
release (using a previous VCs tag). This can be done by calling manually 
the <package>task_diff</package> script (see <xref linkend="statusdump"/>)
</para>

</sect2>

<sect2>
  <title>Apt <filename>sources.list</filename> files in <filename>/etc/blends/</filename></title>
<para>
These files are used by <citerefentry><refentrytitle>blend-gen-control</refentrytitle>
  <manvolnum>1</manvolnum></citerefentry> to
build valid <filename>debian/control</filename> files that contain only
available packages in their dependencies.  This enables building meta
packages for <package>stable</package>, <package>testing</package>, <package>unstable</package> or
even a completely different distribution that has valid
<filename>sources.list</filename> entries.  The file
<filename>/etc/blends/control.list</filename> is used as default for 
<citerefentry><refentrytitle>blend-gen-control</refentrytitle>
  <manvolnum>1</manvolnum></citerefentry> and usually is a symbolic link
(see <citerefentry><refentrytitle>ln</refentrytitle>
  <manvolnum>1</manvolnum></citerefentry>) to
<filename>sources.list.</filename><varname>distribution</varname>.  It might be
changed using the <package>-s </package><varname>dist</varname> option of <citerefentry><refentrytitle>blend-gen-control</refentrytitle>
  <manvolnum>1</manvolnum></citerefentry>.
</para>
<para>
<emphasis>TODO:</emphasis> <emphasis>Either parse the available
<filename>/etc/apt/sources.list</filename> or use a sane <orgname>debconf</orgname>
question to use the "nearest" mirror.</emphasis>
</para>
</sect2>

<sect2>
  <title>Templates in <filename>/usr/share/blends/templates</filename></title>
<para>
The directory <filename>/usr/share/blends/templates</filename> contains templates
that can be used to build a <varname>&lt;blend&gt;</varname><package>-config</package>,
which uses the tools that are contained in the
<package>blends-common</package> package, and are useful to manage
<varname>&lt;blend&gt;</varname> user groups (see <xref linkend="userroles"/>).
</para>
</sect2>
</sect1>

<sect1 id="blends-common">
  <title>Package <package>blends-common</package></title>

<para>
This package creates a common registry for all Blends in
<filename>/etc/blends</filename>.  Each Blend should put the files that are used
into a subdirectory named like the Blend of <filename>/etc/blends</filename>.  The
<package>blends-common</package> package installs a common configuration
file <filename>/etc/blends/blends.conf</filename>, which can be used to influence the
behaviour of the tools described below.  
</para>

<sect2>
  <title><citerefentry><refentrytitle>blend-role</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry></title>
<para>
<variablelist>
<varlistentry>
  <term>NAME</term>
   <listitem><para>
    <package>blend-role</package> - add/remove roles in registered Debian Pure Blend
      </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>SYNOPSIS</term>
   <listitem><para>
    <package>blend-role</package> <varname>add|del</varname> <varname>Blend</varname>
     [<varname>Role</varname>]</para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>DESCRIPTION</term>
   <listitem><para>Add/remove (register/unregister) <varname>Role</varname> for the
   specified <varname>Blend</varname>. If <varname>Role</varname> is not specified, it's
   assumed to be named like <varname>Blend</varname>.</para>
   </listitem>
</varlistentry>

<varlistentry> 
  <term>OPTIONS</term>
   <listitem>
    <variablelist>
    <varlistentry>
     <term><varname>Blend</varname></term>
      <listitem><para>A registered Blend in /etc/blends, for example
      one of <package>med</package>, <package>junior</package>,
            <package>edu</package> or <package>science</package></para>
      </listitem>
      </varlistentry>
    </variablelist>
   </listitem>
</varlistentry>    

<varlistentry> 
  <term>AUTHOR</term>
   <listitem><para>Andreas Tille <email>tille@debian.org</email>, 
   Cosimo Alfarano <email>kalfa@debian.org</email>.</para></listitem>
</varlistentry> 

</variablelist>

</para>
</sect2>

<sect2 id="blend-update-menus">
  <title><citerefentry><refentrytitle>blend-update-menus</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry></title>
<para>
<variablelist>

<varlistentry> 
  <term>NAME</term>
   <listitem><para>
    <package>blend-update-menus</package> - add menu of metapackage to all Blend users</para>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>SYNOPSIS</term>
   <listitem><para>
    <package>blend-update-menus</package> [<varname>--blend Blend</varname> | <varname>--user
    user</varname>]</para>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>DESCRIPTION</term>
   <listitem>
   <para>
   blend-update-menus behaves differently depending on who run the
   command:
   </para>

   <para>
   If it is called by a user, it adds, and keeps updated, menu
   entries for the user who runs it.
   </para>

   <para>
   If it is called by root, it adds and keeps updated user's menu
   entries (see menu package for users' menus) for all users who
   belong to the group of the specified Blend, or only for a specified
   user, depending on which parameter is passed to the script.
   </para>
   </listitem>
 </varlistentry>   

<varlistentry> 
  <term>OPTIONS</term>
   <listitem>
    <variablelist>
      <varlistentry> 
     <term><varname>Blend</varname></term>
      <listitem><para>one of the installed Blends, listed in /etc/blends/, for example
      (if installed: <package>med</package>, <package>junior</package>,
            <package>edu</package> or <package>science</package>.</para>
      </listitem>
    </varlistentry> 
    <varlistentry> 
     <term><varname>user</varname></term>
      <listitem><para>system user</para></listitem>
    </varlistentry> 
    </variablelist>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>AUTHOR</term>
   <listitem><para>Andreas Tille <email>tille@debian.org</email>,
   Cosimo Alfarano <email>kalfa@debian.org</email>.</para></listitem>
</varlistentry> 

</variablelist>

</para>
</sect2>

<sect2>
  <title><citerefentry><refentrytitle>blend-user</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry></title>
<para>

<variablelist>

<varlistentry> 
  <term>NAME</term>
   <listitem><para>
    <package>blend-user</package> - add/remove user to Role of a registered Blend</para>

   </listitem>
</varlistentry> 

<varlistentry> 
  <term>SYNOPSIS</term>
   <listitem><para>
    <package>blend-user</package> <varname>add|del</varname> <varname>Blend</varname> <varname>user</varname> [<varname>Role</varname>]
   </para></listitem>
</varlistentry> 

<varlistentry> 
  <term>DESCRIPTION</term>
   <listitem><para>Add/remove user to a <varname>Role</varname> of the specified <varname>Blend</varname>.

   If <varname>Role</varname> is not specified, it's assumed to be named like
   <varname>Blend</varname></para>
   </listitem>
</varlistentry> 
  
<varlistentry>  
  <term>OPTIONS</term>
   <listitem>
    <variablelist>
      <varlistentry> 
     <term><varname>Blend</varname></term>
      <listitem><para>A registered Blend in /etc/blends, for example
      one of <package>med</package>, <package>junior</package>,
            <package>edu</package> or <package>science</package>.</para>
      </listitem>
    </varlistentry> 
    <varlistentry> 
     <term><varname>user</varname></term>
      <listitem><para>user to add</para></listitem>
    </varlistentry> 
    <varlistentry> 
     <term><varname>Role</varname></term>
      <listitem><para>the role in the <varname>Blend</varname> that <varname>user</varname> will
      assume</para></listitem>
    </varlistentry> 
    </variablelist>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>AUTHOR</term>
   <listitem><para>Andreas Tille <email>tille@debian.org</email>, 
   Cosimo Alfarano <email>kalfa@debian.org</email>.</para></listitem>
</varlistentry> 
</variablelist>
</para>
</sect2>

<sect2>
  <title><citerefentry><refentrytitle>blends.conf</refentrytitle>
  <manvolnum>5</manvolnum></citerefentry></title>
<para>

<variablelist>

<varlistentry> 
  <term>NAME</term>
   <listitem><para>
    <filename>blends.conf</filename> - configuration for Debian Pure Blends registry
   </para></listitem>
</varlistentry> 

<varlistentry> 
  <term>DESCRIPTION</term>
   <listitem><para>This file is sourced from shell scripts inside the Debian
         Pure Blends package <package>blends-common</package> and thus
         it has to follow shell syntax.  The variables that are set
         inside this configuration file can be overridden by special
         Blend configuration files
   <filename>/etc/blends/</filename><varname>Blend&gt;</varname>/<varname>Blend&gt;</varname><filename>.conf</filename>
   for each single Blend.</para></listitem>
</varlistentry> 

<varlistentry> 
  <term>SYNTAX</term>
   <listitem><para>The following variables can be set:</para>
    <variablelist>
      <varlistentry> 
      <term><package>DBBACKEND</package></term>
       <listitem><para>Set the backend for the user role management system.
             Currently the only implemented role management system is
             <package>unixgroups</package> but others might be implemented
             later.  Unsetting this variable leads to use no roles at all.</para>
       </listitem>
       </varlistentry> 
        <varlistentry> 
      <term><package>UPDATEUSERMENU</package></term>
       <listitem><para>If this is set to <package>yes</package>, the user menus of meta
             packages can be created automatically at install time of
             the package if the postinst script of the package allows
             this.  It is suggested to use this option in the specific
             configuration files of a special Debian Pure Blend that
             override the settings of the general configuration file.</para>
       </listitem>
     </varlistentry> 
     <varlistentry> 
      <term><package>SHAREDIR</package></term>
       <listitem><para>Set the base directory for the user role management
             system. While this is more or less a feature for
             debugging this might be also used otherwise. </para>
       </listitem>
     </varlistentry> 
     <varlistentry> 
      <term><package>DRYRUN</package></term>
       <listitem><para>This variable can be set for debugging.  Normally it
             should be left unset (<emphasis>NOT</emphasis> set to <package>false</package>
             or anything else!). If set to <package>true</package> a dry run of
             the tools is performed or <package>echo DRYRUN:</package> would
             print debugging information. </para>
       </listitem>
     </varlistentry> 
     <varlistentry> 
      <term><package>DEBUG</package></term>
       <listitem><para>If set to <package>1</package> debugging mode is switched on.</para></listitem>
     </varlistentry> 

    </variablelist>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>SEE ALSO</term>
   <listitem><para>
    <citerefentry><refentrytitle>blend-role</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry>, <citerefentry><refentrytitle>blend-update-menus</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry>, 
    <citerefentry><refentrytitle>blend-user</refentrytitle>
  <manvolnum>8</manvolnum></citerefentry></para>
   </listitem>
</varlistentry> 

<varlistentry> 
  <term>AUTHOR</term>
   <listitem><para>Andreas Tille <email>tille@debian.org</email>, 
   Cosimo Alfarano <email>kalfa@debian.org</email>.</para></listitem>
</varlistentry> 

</variablelist>
</para>
</sect2>

</sect1>

<sect1 id="developing_normal_packages">
  <title>How to develop new normal packages in Pure Blends</title>
<para>
  Sometimes you may want to develop new packages for your Pure Blend.  It is almost
  identical to creating a new debian package.  But in Blends framework you can do it more
  easily.
</para>
<para>
  Assume that you have already had a well-defined blend.  That is, those necessary files
  like changelog, compat, copyright and control.stub are in your debian/ folder, maybe
  with postinst.stub or prerm.stub for task metapackages.  Notice that, all these control
  and maintainer scripts are just for metapackages defined in your task, not for your new,
  normal packages since they aren't listed in task.
</para>
<para>
  When you want to develop and add new packages, just like debian packaging, you need to
  add package information and parameters in the control file.  However in blends, you should
  add them in debian/control.stub instead of defining their own control file.  For the
  maintainer script files, you need to add them in debian/ folder.   However, notice that
  the maintainer script files should not add ".stub" or blends-dev won't process them.
</para>
<para>
  For example, in blend "foo", we want to add a new normal package "bar".  In debian/control.stub,
  we may have:

<programlisting>
Source: foo
Section: misc
Priority: extra
Maintainer: Foo Team &lt;debian-foo@lists.org&gt;
Uploaders: Foo bar &lt;foo-bar@nowhere.com&gt;
Build-Depends: debhelper (>= 7), blends-dev (>= 0.6.91)
Standards-Version: 3.9.3
Dm-Upload-Allowed: yes
Homepage: http://foo.bar/

Package: foo-bar
Architecture: all
Description: sample blends and packages
</programlisting>
</para>
<para>
  In the control.stub it defines foo blends, and a new package foo-bar.  Then, in the
  debian folder, we can add our own preinst, postinst, prerm, and/or postrm maintainer
  scripts.  However the filename should be foo-bar.preinst, foo-bar.postinst, ... etc.,
  instead of foo-bar.postinst.stub, since foo-bar doesn't exist in tasks.
</para>

<para>
  After those control files are done, you can use

<programlisting>make dist</programlisting>

  to create blends control files, and then use <programlisting>debuild</programlisting>
  to start packaging.
</para>

</sect1>

<sect1 id="git">
  <title>Working with the source repository in <filename>Git</filename></title>
<para>
  Sometimes it might be interesting for developers to check out the
  latest code of the Blend tools or a special Blend code for the meta
  packages.  In <xref linkend="communication"/> the directory layout of the
  <filename>Git</filename>-repository was described.  How to derive the
  Debian packages from this layout?
<variablelist>

<varlistentry>
  <term>Checkout</term>
   <listitem><para>
    For the Blend tools and this documentation
<informalexample>
<programlisting>
  gbp clone git@salsa.debian.org:blends-team/blends.git
</programlisting>
</informalexample>
  
    or for the Debian Pure Blend <varname>BLEND-name</varname>
  
<informalexample>
  <programlisting>
  gbp clone git@salsa.debian.org:blends-team//<varname>BLEND-name</varname>.git
</programlisting>
</informalexample>

    </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Build source package</term>
   <listitem><para>
    Change into the created directory and type
<informalexample>
  <programlisting>
  make -f debian/rules get-orig-source
</programlisting>
</informalexample>

    This creates a <filename>tar.gz</filename> source archive of the packages
    you want to build.  For your personal comfort you can create a
    file <filename>Makefile</filename> in your package source directory containing
 
<informalexample>
  <programlisting>
  #!/usr/bin/make -f 
  include /usr/share/blends-dev/Makefile
</programlisting>
</informalexample>
<programlisting>
    Which enables you to simply say
  </programlisting>
<informalexample>
  <programlisting>
    make dist
  </programlisting>
</informalexample>

    to create the source tarball.
  </para>
   </listitem>
</varlistentry>

<varlistentry>
  <term>Build Debian packages</term>
   <listitem><para>
    gbp buildpackage
   </para>
   </listitem>
</varlistentry>

</variablelist>
</para>
<para>
The current Debian Med packages provide a working example how to use
the tools described below.
</para>
</sect1> 

  <sect1 id="webpagecreation">
  <title>How to create tasks and bugs pages of web sentinel</title>

<para>
In <xref linkend="sentinel"/> the creation of so called tasks
pages <xref linkend="packageslist"/> and bugs pages <xref linkend="bugs"/> was
described. These pages are automatically build by a cron job on blends.debian.org
from the current state of the tasks files in the Git repositories of all registered Blends.
</para>
<para>
To know what a valid <package>&lt;blend-name&gt;</package> might be have a look
into
<filename>/srv/blends.debian.org/webtools/webconf</filename>.
Each Blend has an according config file there.  Leave out
the <filename>.conf</filename> extension and you have a
valid <package>&lt;blend-name&gt;</package>.
</para>
<para>
In case you are planing some more experimental changes there is
another host (sponsored by rackspace.com)
called <package>blends.debian.net</package> which is running a copy of
UDD and also hosts the latest development snapshot of the Blends web
tools.  Just ask Andreas Tille <email>tille@debian.org</email> in case
you like a login on this host.
</para>
<para>
The code which builds web and tasks pages is available in Git at
<filename>https://salsa.debian.org/blends-team/website</filename>.  It is
using the <ulink url="http://genshi.edgewall.org/">Genshi
templating system</ulink> which enables influencing the layout of the pages
by editing the templates in the
<filename>templates</filename> directory.
You can also influence some parameters of the web pages in the
configuration files in the <filename>webconf</filename> directory.
Last but not least you can provide translations for the web pages in
the <filename>po</filename> directory.
</para>
<para>
Please note that the <filename>css</filename> and <filename>js</filename> files which are
influencing the layout of the automatically created pages are in
the same area as the static web pages (see below <xref linkend="staticwebpages"/>).
</para>
</sect1>

  <sect1 id="staticwebpages">
  <title>Editing static web pages of Blends on blends.debian.org</title>
<para>
A very simple entry page is created for each Blend which is linked
from the 
<ulink url="http://blends.debian.org/">list of all Blends</ulink>.
Most probably the maintainers of a Blend want to enhance this page a
bit.  Maintainers of a Blend should care for this index page which currently is just
featuring links to the automatically updated pages as well as an image
which shows the activity of the
<ulink url="http://people.debian.org/~tille/talks/200808_lightning/liststats.pdf">
  relevant mailing list</ulink>.  Maintaining these static pages is
not a "service" which is done for you.  The maintainers of a Blend
should care for this!
</para>
<para>
The static pages are maintained in Git at
<filename>https://salsa.debian.org/blends-team/website</filename> in the
<filename>websites</filename> directory.  A cron job is watching changes
on Salsa and will update the static web pages.
</para>
</sect1>
  <sect1 id="datagathering">
  <title>Description how Blends relevant data are gathered and stored</title>
<para>
All data relevant for Blends are available in
<ulink url="https://wiki.debian.org/UltimateDebianDatabase">Ultimate Debian Database (UDD)</ulink>.
Here is a description of the data gatherers that were explicitly invented to provide
information for usage in Blends websentinel.
</para>

  <sect2 id="ftpnew">
  <title>Packages in Debian ftp new queue</title>
<para>
New packages might reside for some time in the so called new queue until
a member of the ftpmaster team has evaluated the package as fit for the
Debian distribution.  To enable informing our users about that status the
new queue is parsed and the information displayed in the web sentinel pages.
The actual gatherer code can be found in UDD Git and is split into a
<ulink url="https://salsa.debian.org/qa/udd/blob/master/scripts/fetch_ftpnew.sh">Script that fetches the data</ulink>
and a <ulink url="https://salsa.debian.org/qa/udd/blob/master/udd/ftpnew_gatherer.py">Parser which injects the data into UDD</ulink>.
</para>
  </sect2>

  <sect2 id="machine_readable">
  <title>Machine readable data in Git repositories of Blends and some packaging teams</title>
<para>
Several machine readable data of packages are parsed from Git repositories.
There are data that might be more up to date in Git (for instance scientific
publication data or some registry entries).  There are also data for not
yet available packages a Blends team is working on.  To enable referring to
this data some selected Git repositories are parsed.  The parser for Salsa
is available in the
<ulink url="https://salsa.debian.org/blends-team/website/blob/master/misc/machine_readable/fetch-machine-readable_salsa.py">website Git</ulink>
of the Blends project.  It is running in a daily cron job on the host
<ulink url="http://blends.debian.net">blends.debian.net</ulink> and
creates an <ulink url="http://blends.debian.net/_machine-readable/machine-readable.tar.xz">archive</ulink>
with all the machine readable files found in the specified Git repositories.
</para>
<para>
This <ulink url="http://blends.debian.net/_machine-readable/machine-readable.tar.xz">archive</ulink>
is read into UDD by an
<ulink url="https://salsa.debian.org/qa/udd/blob/master/udd/blends_prospective_gatherer.py">UDD
importer script</ulink> in a cron job which is totally independent from the job
that collects the data from Salsa.  Due to the disconnected jobs running on
different hosts there might be some undetermined delay between changing
some metadata in a Git repository and the time when the data are available
in UDD.
</para>
  </sect2>

</sect1>
  </appendix>
